//parsiusti duomenis apie Default miesta
var defaultCity = "gargzdai";
var weatherAPI  = "http://api.openweathermap.org/data/2.5/weather?q=";
var apiKey       = "28f941a283a599657eacbf65563f7beb";

function parsiustiDuomenis(city, weatherAPI, id) {
    let url = weatherAPI + city + "&units=metric&appid=" + id;

    //ajax užklausa
    httpRequest = new XMLHttpRequest();

    httpRequest.onreadystatechange = uzkrautiDuomenis; //kai įvyks užklausa veiks ši funkcija
    httpRequest.open('GET', url);  //užklausos metodas
    httpRequest.send();            //užklausos vykdymas
}

//iškviečiame funkcijas kai suveikia kodas (užsikrauna svetainė)
parsiustiDuomenis(defaultCity, weatherAPI, apiKey);

//duomenis užkrauti į DIV'ą
function uzkrautiDuomenis() {
    if (httpRequest.readyState === XMLHttpRequest.DONE) {
        if (httpRequest.status === 200) {
            //duomenu apdorojimas jeigu TRUE
            var response = JSON.parse(httpRequest.responseText);
            let oruDivai = document.getElementsByClassName('orai');
            oruDivai[0].innerHTML = response.main.temp + " celcius";
        } else {
            //duomenu apdorojimas jei FALSE
            alert("ERROR");
        }
    }
}

//kai paspaudžia miestą, parsiųsti duomenis išnaujo ir užkrauti į DIV'ą
document.getElementById('skuodas').onmouseover = function () { //.addeventListener('onhover',funkcijas);
    parsiustiDuomenis('skuodas', weatherAPI, apiKey);
}
document.getElementById('gargzdai').onmouseover = function () {
    parsiustiDuomenis('gargzdai', weatherAPI, apiKey);
}
document.getElementById('klaipeda').onmouseover = function () {
    parsiustiDuomenis('klaipeda', weatherAPI, apiKey);
}
document.getElementById('taurage').onmouseover = function () {
    parsiustiDuomenis('taurage', weatherAPI, apiKey);
}